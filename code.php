<?php 
// [SECTION] Objects as Variable

    $buildingObj = (object)[

        'name' => 'Caswynn Building',
        'floors' => 8,
        'address' => 'Brgy Sacred Heart, Quezon City Philippines'

    ];

//[SECTION] Objects from Classes
 class Building{
    //Properties
    public $name;
    public $floors;
    public $address;

    public function __construct($name, $floors, $address){

        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    //Method
    public function printName(){

        return "The name of the building is $this->name";
    }

 }
//Instantiate the Building class to create a new building object.

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

// [SECTION] Inheritance and Polymorphism
// The "extends" keywordis used to derive a class from another class.
// A derived class (child) has all the properties and method of the base class.
class Condominium extends Building{
 
    //Polymorphism - Methos inheritied by a derived class that can be ovrriden to have a behavior differend from the method of base class
    public function printName(){

        return "The name of the condominium is $this->name";
    }

}
 
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City');
?>