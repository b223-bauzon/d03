<?php require_once('./code.php') ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03: Clases and Objects</title>

    <h1>Objects from Variable</h1>
    <p><?php var_dump($buildingObj); ?></p>
    <p><?php echo $buildingObj->name; ?></p>

    <h1>Objects from Classes</h1>
    <p><?php var_dump($building); ?></p>
    <p><?php echo $building->name; ?></p>
    <p><?php echo $building->printName(); ?></p>

    <h1>Inheritance (Condominium Object)</h1>
    <p><?php var_dump($condominium); ?></p>

    <h2>Polymorphism</h2>
    <p><?php echo $condominium->printName(); ?></p>

</head>
<body>
    
</body>
</html>